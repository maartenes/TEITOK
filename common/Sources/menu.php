<?php
	// Script to display the menubar
	// Maarten Janssen, 2015
	// Default items: {%Home}, {%Search}, {%About}, {%!help}

	$checkshared = preg_replace("/.*\/([^\/]+)\/?/", "\\1", getenv('TT_SHARED'));
	if ( $checkshared == $foldername ) { 
		$isshared = 1;
	};

	if (  count(getset('languages/options', array())) > 1 ) {
		$sep = "";
		foreach ( getset('languages/options', array()) as $key => $item ) {
			if ( $item['menu'] == "" ) continue;
			if ( $key == $lang ) $langstxt .= "<span class=langon>$sep{$item['menu']}</span>";
			else {
				if ( getset('languages/prefixed') != '' ) {
					$langurl = "$rooturl$key/index.php?".$_SERVER['QUERY_STRING'];
				} else {
					if ( preg_match("/\?/", $_SERVER['REQUEST_URI']) ) $ss = "&"; else $ss = "?";
					$langurl = $_SERVER['REQUEST_URI']."{$ss}lang=$key";
				};
				$langstxt .= "$sep<span class=langoff><a href='$langurl'>{$item['menu']}</a></span>";
			};
			$sep = " | ";
		};
		if ( getset('languages/position') != '' ) {
			$smarty->assign(getset('languages/position'), $langstxt);
		} else $menu .= "<div stle='margin-top: 0px; margin-bottom: 20px;'>$langstxt</div>";	
	};

	$menname = getset('menu/name');
	if ( $menname  != '' ) {
		if ( $menname == "[title]" )
			$menu .= "<p class='title'>{%".getset('defaults/title/display')."}</p>";
		else if ( $menname == "[short]" )
			$menu .= "<p class='title'>{%".getset('defaults/title/short', getset('defaults/title/display'))."}</p>";
		else $menu .= "<p class='title'>{%$menname}</p>";
	};

	if ( getset('menu/title') == "none" ) $nomentit = 1; # Do nothing
	else if ( getset('menu/title') ) {
		$menutitle = getset('menu/title');
		if ( $menutitle == "[title]" ) $menutitle = getset('defaults/title/display', "{%$menutitle}");
		if ( getset('menu/link') != '' ) $menutitle = "<a href='".getset('menu/link')."'>$menutitle</a>";
		if ( getset('menu/title') != "[none]" ) $menu .= "<p class='header'>$menutitle</p>";
	} else $menu .= "<p>{%Main Menu}</p>";
	
    $menu .= "<ul style='text-align: left'>";
        	
    if ( is_array($moremenu) ) 
    	foreach ( $moremenu as $key => $val ) {
    		$settings['menu']['itemlist'][$key] = $val;
    	};
        	
    foreach ( getset('menu/itemlist', array()) as $key => $item ) { 
		$scl = $scli = $trgt = $link = "";
    	if ( !is_array($item) ) continue; # Skip attributes
    	# For shared menu item
    	if ( $item['nolocal'] && $isshared ) {
    		continue; # Skip items that should not be available for the shared project
    	};
    	if ( $tmp = $item['check'] ) {
    	 if ( ( $tmp == "php" && !file_exists("Sources/$key.php") ) || ( $tmp == "html" && !file_exists("Pages/$key.html") )  || ( !file_exists($tmp) ) ) {
			if ( $username ) {
				$item['admin'] = 1;
			} else continue; 
    	 }
    	};
    	if ( $item['xp'] ) {
    		$link = current($settingsxml->xpath($item['xp']));
    		if ( !$link ) continue; # Do not show the item if the XPath does not match
    	} else if ( $item['link'] ) $link = $item['link'];
    	else if ( substr($key,0,4) == "http" ) {
    		$link = $key;
    	} else {
    		$link = "{$tlpr}index.php?action=$key";    		
    	};
    	if ( $item['target'] ) $trgt = " target=\"{$item['target']}\"";
    	else if ( is_array($link) && $link['target'] ) $trgt = " target=\"{$link['target']}\"";
		if ( $item['class'] ) $scli .= " {$item['class']}";
    	if ( preg_replace("/&.*/", "", $key) == $action ) {
    		$scl .= " selected"; 
    		$scli .= " active"; 
    	};
    	if ( $scl ) $scl = " class='$scl'"; 
    	if ( $scli ) $scli = " class='$scli'"; 
		$itemtxt = $item['display'] or $itemtxt = $key;
		if ( $item['title'] ) $scl .= " title=\"{$item['title']}\"";
    	if ( $item['type'] == "separator" ) {
    		$menu .= "</ul><hr><ul style='text-align: left'>";
    	} else if ( $item['type'] == "header" ) {
    		$mhead = "{%$itemtxt}";
    		if ( $item['link'] ) $mhead = "<a href='{$item['link']}'>{%$itemtxt}</a>";
    		$menu .= "</ul><h3>$mhead</h3><ul style='text-align: left'>";
		} else if ( $item['admin'] ) {
    		if ( $item['admin'] == 1 || $user['permissions'] == "admin" ) {
	    		$adminitems .= "<ul style='text-align: left'><li $scli><a$trgt href='$link'$scl>{%$itemtxt}</a></ul>";
	    	};
    	} else {
    		$menu .= "<li><a$trgt href='$link'$scl>{%$itemtxt}</a>";
    	};
	};
   $menu .= "</ul>";
	
  	if ( $username ) {
  	
		$shortuserid = $user['short'];
	 	$usertype = "user"; if ( $user['projects'] == "all" ) $usertype = "<span title='server-wide user'>guser</span>";
  		$menu .= "<hr>$usertype: <a href='index.php?action=user'>$shortuserid</a><hr>";
  		$tmp = ""; if ( $action == "admin" ) $tmp = "class=\"selected\""; 
  		$menu .= "<ul style='text-align: left'><li><a href='{$tlpr}index.php?action=admin' $tmp>Admin</a></ul>";
  		$menu .= "<ul style='text-align: left'><li><a target=help href='http://www.teitok.org/index.php?action=help'>Help</a></ul>";
		if ( ( file_exists("$bindir/tt-cqp") && getset("cqp") != '' ) || getset("defaults/tt-cqp") ) $menu .= "<ul style='text-align: left'><li><a href='index.php?action=classify'>Custom annotation</a></ul>"; 
  		$tmp = ""; if ( $action == "files" ) $tmp = "class=\"selected\""; 
  		if ( is_dir("xmlfiles") ) $menu .= "<ul style='text-align: left'><li><a href='{$tlpr}index.php?action=files' $tmp>XML Files</a></ul>";
		$qfldr = preg_replace("/[^a-z0-9]/", "", strtolower($username));
		$qfn = "Users/$qfldr/queries.xml";
		if ( file_exists($qfn) || $_SESSION['queries'] ) $menu .= "<ul style='text-align: left'><li><a href='index.php?action=querymng'>{%Query Manager}</a></li></ul>";

  		$menu .= $adminitems;
  		
	} else if ( $_SESSION['extid'] ) {

		if ( !is_array(getset('permissions')) ) $settings['permissions'] = array();
		if ( ! getset('permissions/shibboleth') ) $settings['permissions']['shibboleth'] = array ( "display" => "visitor" ); # Always allow Shibboleth login
		foreach ( $_SESSION['extid'] as $key => $val ) { 
		if ( is_array(getset("permissions/$key") ) ) { 
			$tmp = getset("permissions/$key");
			$idtype = $key;
			$idname = $tmp['display'] or $idname = strtoupper($idtype); 
			$idaction = $tmp['login'] or $idaction = "extuser"; 
			$shortuserid = $_SESSION['extid'][$idtype];
			$userid = $shortuserid;
			$menu .= "<hr>$idname: <a href='index.php?action=$idaction'>$shortuserid</a><hr>";
			$qfldr = preg_replace("/[^a-z0-9]/", "", strtolower($userid));
			$qfn = "Users/$qfldr/queries.xml";
			if ( file_exists($qfn) || $_SESSION['queries'] ) $menu .= "<ul><li><a href='index.php?action=querymng'>{%Query Manager}</a></li></ul>";
			foreach ( $tmp['functions'] as $key => $func ) {
				$menu .= "<ul style='text-align: left'><li><a href='{$tlpr}index.php?action={$func['key']}' $tmp>{%{$func['display']}}</a></ul>";
			};
		}; }; 
		if ( !$idtype && !getset('menu/nologin') ) {
	  		$menu .= "<ul style='text-align: left'><li><a href='{$tlpr}index.php?action=login'><i>Login</i></a></ul>";
		};
	} else if ( !getset('menu/nologin')  ){
		$shortuserid = "guest";
  		$tmp = ""; if ( $action == "login" ) $tmp = "class=\"selected\""; 
  		$menu .= "<ul style='text-align: left'><li><a href='{$tlpr}index.php?action=login' $tmp>Login</a></ul>";
	};
        	
        
?>