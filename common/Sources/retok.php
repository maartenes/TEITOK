<?php
	// Script to insert and remove <tok> 
	// from an XML file
	// (c) Maarten Janssen, 2015

	check_login();

	$fileid = $_POST['cid'] or $fileid = $_GET['cid'];
	$tokid = $_POST['tid'] or $tokid = $_GET['tid'];
	$nextid = $tokid;	
		
	if ( $fileid && $tokid ) { 
	
		if ( !file_exists("$xmlfolder/$fileid") ) { 
			print "No such XML File: $xmlfolder/$fileid"; 
			exit;
		};
		
		$file = file_get_contents("$xmlfolder/$fileid"); 
		
		# In which direction to place the node - TODO: add those defaults?
		$nodedir = $_GET['dir']; # or $nodedir = "before";
		# Where to place the node
		$nodepos = $_GET['pos']; # or $nodepos = "left";
		# What node to place
		$nodetype = $_GET['node']; # or $nodetype = "tok";
		
		$tmpid = " id=\"torenew\" ";
		$ntid = "torenew";
		
		if ( $nodetype == "par" ) {
			$newnode = "<sb$tmpid/>";
			$nodepos = "right";
		} else if ( $nodetype == "dtok" ) {
			$newnode = "<dtok$tmpid/>";
			$nodepos = "inside";
		} else if ( $nodetype == "lb" ) {
			$newnode = "<lb$tmpid/>";
			$nodepos = "left";
		} else if ( $nodetype == "s" ) {
			$newnode = "<s$tmpid/>";
			$nodepos = "left";
		} else if ( $nodetype == "note" ) {
			$newnode = "<note$tmpid/>";
			$nodedir = "after";
			$nodepos = "left";
			$goto = urlencode("index.php?action=elmedit&cid=$fileid&tid=newid");
		} else if ( $nodetype ) {
			$nodetype = asciify($nodetype);
			$newnode = "<$nodetype$tmpid/>";
			$nodepos = $_GET['pos'];
		} else {
			## By default, insert a token with no written form
			$newnode = "<tok$tmpid><ee/></tok>";
		};
		
		$nodetype = substr($tokid,0,1);
	
		if ( $nodepos == "inside" ) {
			if ( !$newnode )  { $newnode = "<dtok/>"; };
			
			preg_match( "/(<tok ([^>]*)id=\"$tokid\"(.*?))<\/tok>/", $file, $matches );
			$fromtok = $matches[0]; $totok = $fromtok;
			$dtoknr = $_GET['cnt'] or $dtoknr = 1;
			for ( $i=1; $i<=$dtoknr; $i++ ) {
				$totok = str_replace('</tok>', $newnode.'</tok>', $totok);
			}; $fromtok = preg_quote($fromtok, '/');
			
			$file = preg_replace ( "/$fromtok/", "$totok", $file );
			$ntid = $tokid;
		
		} else if ( $nodedir == "before" ) {
		
			if ( $nodetype == "w" ) {
				# print "<p>Adding before w = $tokid";
				if ( $nodepos == "left" ) {
					$file = preg_replace ( "/(\s+)(<tok ([^>]*)id=\"$tokid\")/", "$newnode$1$2", $file );
				} else if ( $nodepos == "right" ) {
					$file = preg_replace ( "/(<tok ([^>]*)id=\"$tokid\")/", "$newnode$1", $file );
				} else if ( $nodepos == "glued" ) {
					$file = preg_replace ( "/(\s+)(<tok ([^>]*)id=\"$tokid\")/", "$newnode$1$2", $file );
				} else {
					$file = preg_replace ( "/(<tok ([^>]*)id=\"$tokid\")/", "$newnode $1", $file );
				};
			} else {
				# print "<p>Adding before non-w = $tokid";
				if ( $nodepos == "left" ) {
					$file = preg_replace ( "/ (<([^>]*) id=\"$tokid\")/", "$newnode $1", $file );
				} else if ( $nodepos == "right" ) {
					$file = preg_replace ( "/(<([^>]*) id=\"$tokid\")/", "$newnode$1", $file );
				} else if ( $nodepos == "glued" ) {
					$file = preg_replace ( "/ (<([^>]*) id=\"$tokid\")/", "$newnode$1", $file );
				} else {
					$file = preg_replace ( "/(<([^>]*) id=\"$tokid\")/", "$newnode $1", $file );
				};
			};
			## Move the edit to the previous token
			if ( preg_match("/<tok/", $newnode) ) {
				// just stay if we have token - it will get right after renumbering
				$ntid = $tokid;
			} else {
				// find the next token, which will turn correct after renumbering
				if ( preg_match ("/ id=\"$tokid\".*?<tok.*? id=\"(.*?)\"/", $file, $matches) ) {
					$ntid = $matches[1];
				};
			};
		
		} else if ( $nodedir == "after" ) {	

			if ( $nodetype == "w" ) {
				print "<p>Adding after w = $tokid";
				if ( $nodepos == "left" ) {
					$file = preg_replace ( "/(<tok ([^>]*)id=\"$tokid\".*?<\/tok>)/", "$1$newnode", $file );
				} else if ( $nodepos == "right" ) {
					$file = preg_replace ( "/(<tok ([^>]*)id=\"$tokid\".*?<\/tok>) /", "$1 $newnode", $file );
				} else if ( $nodepos == "glued" ) {
					$file = preg_replace ( "/(<tok ([^>]*)id=\"$tokid\".*?<\/tok>) /", "$1$newnode", $file );
				} else {
					$file = preg_replace ( "/(<tok ([^>]*)id=\"$tokid\".*?<\/tok>)/", "$1 $newnode", $file );
				};
				
				// just increase the number if we have token
				$ntid = $nodetype.'-'.(substr($tokid,2)+1);
			} else {
				print "<p>Adding after non-w = $tokid";
				if ( $nodepos == "left" ) {
					$file = preg_replace ( "/(<*([^>]*) id=\"$tokid\"[^>]*\/>)/", "$1$newnode", $file );
				} else if ( $nodepos == "right" ) {
					$file = preg_replace ( "/(<*([^>]*) id=\"$tokid\"[^>]*\/>) /", "$1 $newnode", $file );
				} else if ( $nodepos == "glued" ) {
					$file = preg_replace ( "/(<*([^>]*) id=\"$tokid\"[^>]*\/>) /", "$1$newnode", $file );
				} else {
					$file = preg_replace ( "/(<*([^>]*) id=\"$tokid\"[^>]*\/>)/", "$1 $newnode", $file );
				};
				if ( preg_match ("/ id=\"$tokid\".*?<tok.*? id=\"(.*?)\"/", $file, $matches) ) {
					$ntid = $matches[1];
				};
			};
		};
		
		$maintext .= "<p>Added ".htmlentities($newnode)." $nodepos $tokid"; 
		
		if ( $goto ) $newurl = "&nexturl=$goto";
		$nexturl = "index.php?action=renumber&cid=$fileid&tid=$ntid&dir=$nodedir$newurl";
		
		saveMyXML($file, $fileid);
				
		$maintext .= "<hr><p>Your tok has been inserted - reloading to <a href='$nexturl'>the edit page</a>";
		$maintext .= "<script langauge=Javasript>top.location='$nexturl';</script>";		
	
	} else {
		print "<p>Please go play outside"; exit;
	};
	
?>