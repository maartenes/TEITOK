<?php

# A class to run python under an Apache managed venv

class VENV
{
	var $venvdir;

	function __construct($venvdir = null) {	
		# Establish where to store the venv
		global $sharedfolder;

		if ( $venvdir ) $this->venvdir = $venvdir;		
		else {
			$this->venvdir = getset("defaults/base/venv");
			if ( !$this->venvdir ) {
				if ( $sharedfolder ) $this->venvdir = "$sharedfolder/Resources/venv";
				else $this->venvdir = "Resources/venv";
			};
		};
				
		# Create a new venv if none exists
		if ( !file_exists( $this->venvdir ) ) {
			shell_exec("python -m venv $this->venvdir");
		};
	}
	
	function installmod ( $modules ) {
		foreach ( explode(",", $modules) as $module ) {
			if ( $this->checkmod($module) ) continue; # already installed
			
			$maintext .= shell_exec("$this->venvdir/bin/pip install $module");
		};
	}

	function checkmod ( $module ) {
		# Check whether a module is installed
		$test = shell_exec("import $module; print(\"test\")");
		if ( $test == "test" ) return true;
		return false;
	}
		
	function version ( ) {
		return shell_exec("$this->venvdir/bin/python --version");
	}
	
	function path ( ) {
		return $this->venvdir;
	}
	
	function exec ( $script ) {
		global $debug, $maintext;
		$cmd = "$this->venvdir/bin/python $script";
		if ( $debug ) $maintext .= "<p>VENV command: $cmd";
		return shell_exec($cmd);
	}
	
};

?>